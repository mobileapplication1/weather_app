import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        title: 'Responsive and adaptive UI in Flutter',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: Scaffold(
        backgroundColor: Colors.blueAccent,
        body: ListView(
          children: <Widget>[
            Container(
              width: double.infinity,
              child: Text(""
              ),
            ),
            Container(
              width: double.infinity,
              child: Text("จ.ชลบุรี",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 35, color: Colors.white,)
              ),
            ),
            Container(
              width: double.infinity,
              child: Text("ส่วนมากปลอดโปร่ง",
                textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, color: Colors.white,)
              ),
            ),
            Container(
              width: double.infinity,
              child: Text("23°",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 90, color: Colors.white,)
              ),
            ),
            Container(
              width: double.infinity,
              child: Text("    วันอาทิตย์                                          29       19",
                textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 20, color: Colors.white,)),
            ),
            Divider(
              color: Colors.white,
            ),
            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  weatherNow(),
                  weather21PM(),
                  weather22PM(),
                  weather23PM(),
                  weather00AM(),
                  weather01AM(),
                  weather02AM()
                ],
              ),
            ),
            Divider(
              color: Colors.white,
            ),
            Container(
              margin: const EdgeInsets.only(top: 8, bottom: 8),
              child: Column(
                children: <Widget>[
                  mondayReportListTile(),
                  tuesdayReportListTile(),
                  wednesdayReportListTile(),
                  thursdayReportListTile(),
                  fridayReportListTile(),
                  saturdayReportListTile(),
                  sundayReportListTile(),
                ],
              ),
            ),
            Divider(
              color: Colors.white,
            ),
            Container(
              width: double.infinity,
              child: Text("วันนี้: สภาพอากาศขณะนี้มีส่วนมากปลอดโปร่ง มีอุณหภูมิ 23° พยากรณ์อากาศอุณหภูมิสูงสุดของวันนี้อยู่ที่ 29°",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, color: Colors.white,)
              ),
            ),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.newspaper_rounded),
              label: 'weather channel',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.navigation),
              label: 'current location',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.list),
              label: 'weather list',
            ),
          ],
        ),
        ),
      ),
    );
  }
}
 //widget today time weather
  Widget weatherNow(){
    return Column(
        children: <Widget>[
          Text("ตอนนี้",
            style: TextStyle(color: Colors.white),
          ),
          IconButton(
            icon: Icon(
              Icons.nightlight,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
          Text("23°",
            style: TextStyle(color: Colors.white),
          ),
        ],
    );
  }
Widget weather21PM(){
  return Column(
    children: <Widget>[
      Text("21",
        style: TextStyle(color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("22°",
        style: TextStyle(color: Colors.white),
      ),
    ],
  );
}
Widget weather22PM(){
  return Column(
    children: <Widget>[
      Text("22",
        style: TextStyle(color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("22°",
        style: TextStyle(color: Colors.white),
      ),
    ],
  );
}
Widget weather23PM(){
  return Column(
    children: <Widget>[
      Text("23",
        style: TextStyle(color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("21°",
        style: TextStyle(color: Colors.white),
      ),
    ],
  );
}
Widget weather00AM(){
  return Column(
    children: <Widget>[
      Text("00",
        style: TextStyle(color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("21°",
        style: TextStyle(color: Colors.white),
      ),
    ],
  );
}
Widget weather01AM(){
  return Column(
    children: <Widget>[
      Text("01",
        style: TextStyle(color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.wb_cloudy,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("21°",
        style: TextStyle(color: Colors.white),
      ),
    ],
  );
}
Widget weather02AM(){
  return Column(
    children: <Widget>[
      Text("02",
        style: TextStyle(color: Colors.white),
      ),
      IconButton(
        icon: Icon(
          Icons.nights_stay,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      Text("21°",
        style: TextStyle(color: Colors.white),
      ),
    ],
  );
}

//widget weather day report
Widget mondayReportListTile() {
  return ListTile(
    leading: Icon(
        Icons.cloud ,
        color: Colors.white),
    title:Text("วันจันทร์                                           28      19",
      style: TextStyle(color: Colors.white,fontSize: 18),
    ),
  );
}
Widget tuesdayReportListTile() {
  return ListTile(
    leading: Icon(
        Icons.cloudy_snowing,
        color: Colors.white),
    title:Text("วันอังคาร                                          26     22",
      style: TextStyle(color: Colors.white,fontSize: 18),
    ),
  );
}
Widget wednesdayReportListTile() {
  return ListTile(
    leading: Icon(
        Icons.sunny,
        color: Colors.white),
    title:Text("วันพุธ                                                31     21",
      style: TextStyle(color: Colors.white,fontSize: 18),
    ),
  );
}
Widget thursdayReportListTile() {
  return ListTile(
    leading: Icon(
        Icons.sunny,
        color: Colors.white),
    title:Text("วันพฤหัสบดี                                      31     21",
      style: TextStyle(color: Colors.white,fontSize: 18),
    ),
  );
}
Widget fridayReportListTile() {
  return ListTile(
    leading: Icon(
        Icons.cloudy_snowing,
        color: Colors.white),
    title:Text("วันศุกร์                                              27     20",
      style: TextStyle(color: Colors.white,fontSize: 18),
    ),
  );
}
Widget saturdayReportListTile() {
  return ListTile(
    leading: Icon(
        Icons.cloud,
        color: Colors.white),
    title:Text("วันเสาร์                                              26     19",
      style: TextStyle(color: Colors.white,fontSize: 18),
    ),
  );
}
Widget sundayReportListTile() {
  return ListTile(
    leading: Icon(
        Icons.thunderstorm,
        color: Colors.white),
    title:Text("วันอาทิตย์                                         25     21",
      style: TextStyle(color: Colors.white,fontSize: 18),
    ),
  );
}
